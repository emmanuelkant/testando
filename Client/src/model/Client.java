package model;

import java.util.Date;

/**
 * Classe que armazena os dados do cliente.
 * 
 * @author Emmanuel Kant
 *
 */
public class Client {
	
	private String name;
	private String cpf;
	private Date birthDay;
	
	public Client(String name, String cpf, Date birthDay) {
		this.name = name;
		this.cpf = cpf;
		this.birthDay = birthDay;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getCpf() {
		return cpf;
	}
	
	public void setCpf(String cpf) {
		this.cpf = cpf;
	}
	
	public Date getBirthDay() {
		return birthDay;
	}
	
	public void setBirthDay(Date birthDay) {
		this.birthDay = birthDay;
	}
	
}
